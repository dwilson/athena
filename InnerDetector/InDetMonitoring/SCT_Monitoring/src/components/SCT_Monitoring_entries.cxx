#include "../SCTErrMonAlg.h"
#include "../SCTHitEffMonAlg.h"
#include "../SCTLorentzMonAlg.h"
#include "../SCTTracksMonAlg.h"
#include "../SCTHitsNoiseMonAlg.h"


using namespace SCT_Monitoring;

DECLARE_COMPONENT( SCTErrMonAlg )
DECLARE_COMPONENT( SCTHitEffMonAlg )
DECLARE_COMPONENT( SCTLorentzMonAlg )
DECLARE_COMPONENT( SCTTracksMonAlg )
DECLARE_COMPONENT( SCTHitsNoiseMonAlg )

