#!/bin/bash
# art-description: art job for InDetPhysValMonitoring, Single muon 10GeV
# art-type: grid
# art-input: user.keli:user.keli.mc16_13TeV.422034.ParticleGun_single_mu_Pt10GeV_Rel22073
# art-input-nfiles: 10
# art-cores: 4
# art-memory: 4096
# art-include: main/Athena
# art-include: 24.0/Athena
# art-output: physval*.root
# art-output: *.xml 
# art-output: art_core_0
# art-output: dcube*
# art-html: dcube_shifter_last

#RDO is made at rel 22.0.73
#reference plots are made at rel 22.0.73

artdata=/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art
dcubeRef=${artdata}/InDetPhysValMonitoring/ReferenceHistograms/nightly_references/2024-06-01T2101/physval_mu10GeV_reco_2024-06-01T2101.root

script=test_MC_mu0_reco.sh

echo "Executing script ${script}"
echo " "
"$script" ${ArtProcess} ${ArtInFile} ${dcubeRef}
