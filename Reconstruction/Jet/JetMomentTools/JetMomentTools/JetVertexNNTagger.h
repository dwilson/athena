// this file is -*- C++ -*-
/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#ifndef JETVERTEXNNTAGGER_H
#define JETVERTEXNNTAGGER_H

///
/// \class JetVertexNNTagger
///
/// Creates a new JetContainer by doing a shallow copy of an input JetVector
///
/// This tool implements the IJetProvider interface. The JetContainer it returns is build by
/// doing a shallow copy of an input JetVector.
/// The JetVector key is also a Property of the tool.
/// 

#include <string>
#include <vector>
#include <utility>
#include <memory>
#include <functional>
#include <optional>

#include "AsgTools/PropertyWrapper.h"
#include "AsgTools/AsgTool.h"
#include "AsgDataHandles/ReadHandleKey.h"
#include "AsgDataHandles/ReadDecorHandleKey.h"
#include "AsgDataHandles/WriteDecorHandleKey.h"
#include "JetInterface/IJetDecorator.h"
#include "JetMomentTools/NNJvtBinning.h"

#include "xAODJet/JetContainer.h"
#include "xAODTracking/VertexContainer.h"

#include "lwtnn/generic/FastGraph.hh"

namespace JetPileupTag {

  // Because there is no SystemOfUnits.h in AnalysisBase
  constexpr float GeV=1e3;

  class JetVertexNNTagger
    : public asg::AsgTool,
      virtual public IJetDecorator
  {
    ASG_TOOL_CLASS(JetVertexNNTagger,IJetDecorator)

    


    public:
      /// Constructor with a tool name
      JetVertexNNTagger(const std::string& name);
      /// Destructor
      ~JetVertexNNTagger();

      // Called in parent initialize()
      virtual StatusCode initialize() override;

      // Inherited method to decorate a jet container
      virtual StatusCode decorate(const xAOD::JetContainer& jetCont) const override;


      struct TrackMomentStruct {
          std::vector<int> numTrk;
          std::vector<float> trkWidth;
          std::vector<float> sumPtTrk;

          TrackMomentStruct(std::vector<int> n, std::vector<float> w, std::vector<float> s)
              : numTrk(n), trkWidth(w), sumPtTrk(s) {}
      };

      struct DTrackMomentStruct {

          std::vector<int> dNumTrk;
          std::vector<float> dTrkWidth;
          std::vector<float> dRpt;

          DTrackMomentStruct(std::vector<int> dN = {}, std::vector<float> dW = {}, std::vector<float> dR = {})
              : dNumTrk(std::move(dN)), dTrkWidth(std::move(dW)), dRpt(std::move(dR)) {}
      };

      struct OrderedTrackMoment {
          std::vector<int> numTrk;
          std::vector<float> trkWidth;
          std::vector<float> rpt;
          

          
          OrderedTrackMoment(std::vector<int> n = {}, std::vector<float> w = {}, std::vector<float> r = {})
              : numTrk(std::move(n)), trkWidth(std::move(w)), rpt(std::move(r)) {}

      };


    private:

      // Retrieve hard scatter vertex for its index. Return nullptr if one cannot be found
      const xAOD::Vertex *findHSVertex(const xAOD::VertexContainer& vertices) const;

      // Evaluate JVT from Rpt and JVFcorr
      float evaluateJvt(const std::vector<float>& features) const;


      /// Internal members for interpreting jet inputs
      /// and NN configuration
      std::unique_ptr<lwt::generic::FastGraph<double> > m_lwnn {nullptr};
      // The Jvt bins and cut map
      NNJvtCutMap m_cutMap;

      // Generically needed for moment tools
      Gaudi::Property<std::string> m_jetContainerName{this,"JetContainer", "", "SG key for the input jet container"};
      Gaudi::Property<bool> m_suppressInputDeps{this, "SuppressInputDependence", false, "Will JVFCorr and SumPtTrk be created in the same algorithm that uses this tool?"};
      Gaudi::Property<bool> m_suppressOutputDeps{this, "SuppressOutputDependence", false, "Ignore creating the output decoration dependency for data flow; for analysis"};

      Gaudi::Property<bool> m_useTrkAugNN{this, "UseTrkAugNN", false, "Flag to select whether to use the Track-Augmented NN configuration"};


      // NN configuration
      Gaudi::Property<std::string> m_TrkAugNNConfigDir{this, "TrkAugNNConfigDir", "/eos/home-d/dwilson/QT/QT_full_stat/NNJvt_configs", "PathResolver-accessible directory holding config files"};
      Gaudi::Property<std::string> m_TrkAugNNParamFileName{this, "TrkAugNNParamFile", "output_hlt_nnjvt_converted.json", "Name of json file containing network parameters"};
      Gaudi::Property<std::string> m_TrkAugNNCutFileName{this, "TrkAugNNCutFile", "cut_file_metrics_35_trigger.json", "Name of json file containing network parameters"};
      Gaudi::Property<std::string> m_NNConfigDir{this,"NNConfigDir", "JetPileupTag/NNJvt/2022-03-22", "PathResolver-accessible directory holding config files"};
      Gaudi::Property<std::string> m_NNParamFileName{this,"NNParamFile", "NNJVT.Network.graph.Offline.Nonprompt_All_MaxWeight.json", "Name of json file containing network parameters"};
      Gaudi::Property<std::string> m_NNCutFileName{this,"NNCutFile", "NNJVT.Cuts.FixedEffPt.Offline.Nonprompt_All_MaxW.json", "Name of json file containing network parameters"};


      // Additional steering 
      Gaudi::Property<float> m_maxpt_for_cut{this,"MaxPtForCut", 60*GeV, "Jet pt above which no cut is applied"};


      // Access to inputs from StoreGate
      SG::ReadHandleKey<xAOD::VertexContainer> m_vertexContainer_key{this, "VertexContainer", "PrimaryVertices", "SG key for input vertex container"};
      SG::ReadDecorHandleKey<xAOD::JetContainer> m_jvfCorrKey{this, "JVFCorrName", "JVFCorr", "SG key for input JVFCorr decoration"};
      SG::ReadDecorHandleKey<xAOD::JetContainer> m_sumPtTrkKey{this, "SumPtTrkName", "SumPtTrkPt500", "SG key for input SumPtTrk decoration"};
      SG::ReadDecorHandleKey<xAOD::JetContainer> m_trkWidthKey{this, "TrackWidthName", "TrackWidthPt1000", "SG key for input TrackWidth decoration"};
      SG::ReadDecorHandleKey<xAOD::JetContainer> m_numTrkKey{this, "NumTrkName", "NumTrkPt1000", "SG key for input NumTrk decoration"};


      SG::WriteDecorHandleKey<xAOD::JetContainer> m_jvtKey{this, "JVTName", "NNJvt", "SG key for output JVT decoration"};
      SG::WriteDecorHandleKey<xAOD::JetContainer> m_rptKey{this, "RpTName", "NNJvtRpt", "SG key for output RpT decoration"};
      SG::WriteDecorHandleKey<xAOD::JetContainer> m_passJvtKey{this, "passJvtName", "NNJvtPass", "SG key for output pass-JVT decoration"};


      //new nnjvt input variables 

      SG::WriteDecorHandleKey<xAOD::JetContainer> m_rptPerVertexKey{this, "rptPerVertexName", "RPtTrkPt500", "SG key for input per vertex RPtTrkPt decoration, vector inputs ordered by sum pt of tracks assigned to jet"};
      SG::WriteDecorHandleKey<xAOD::JetContainer> m_DtrkWidthKey{this, "DTrackWidthName", "DTrackWidthPt1000", "SG key for input DTrackWidth decoration"};
      SG::WriteDecorHandleKey<xAOD::JetContainer> m_DnumTrkKey{this, "DNumTrkName", "DNumTrkPt1000", "SG key for input DNumTrk decoration"};

      SG::WriteDecorHandleKey<xAOD::JetContainer> m_DrptPerVertexKey{this, "DrptPerVertexName", "DRPtTrkPt500", "SG key for input DRPtTrkPt decoration"};


      //sorted track moments, write
      SG::WriteDecorHandleKey<xAOD::JetContainer> m_TrkWidthSortedKey{this, "TrkWidthSortedName", "SumPtTrkOrderedTrackWidthPt1000", "SG key for input TrackWidth decoration, vector inputs ordered by sum pt of tracks assigned to jet"};
      SG::WriteDecorHandleKey<xAOD::JetContainer> m_NumTrkSortedKey{this, "NumTrkSortedName", "SumPtTrkOrderedNumTrkPt1000", "SG key for input NumTrk decoration, vector inputs ordered by sum pt of tracks assigned to jet"};

      // resort ordering of jet moment vectors according to the descending sumtrkpt

      std::vector<size_t> get_resorted_vertex_indices(const std::vector<float>& jet_sumpt_per_vertex, const xAOD::Vertex* HSvertex) const; 

      OrderedTrackMoment get_sorted_track_moments(const TrackMomentStruct& moments, const std::vector<size_t>& resorted_vertex_indices, float jetPt) const; 

      DTrackMomentStruct calculatePerVertexDifferences(const OrderedTrackMoment& sortedTrackMoments, size_t numVertices) const;

  };

}
#endif
